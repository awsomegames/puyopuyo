# Description
A practice attempt to simulate a Puyo Puyo game. Be careful, these Puyos are from space! 🌌

# How to run and play the game
## Option 1
- Go to [GameExecutable](GameExecutable) folder and download the [SpacePuyoPuyoInstaller.exe](GameExecutable/SpacePuyoPuyoInstaller.exe).
- Run the file and follow the Wizard to install and run the game.
## Option 2
- Go to [LatestBuild](LatestBuild) folder and download all its content.
- Unzip your download and run the **PuyoPuyo.exe** file.

# Multiplayer
- You play with the Keyboard.
- You may **join** a **second player** to the game by connecting a **Gamepad** and pressing a button.

# Images
I don't want to spoil the game, please play it to live the experience.  :)
