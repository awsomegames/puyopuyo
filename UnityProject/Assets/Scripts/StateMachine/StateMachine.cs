using System.Collections;
using System.Collections.Generic;
using PuyoStates;
using UnityEngine;

public class StateMachine : MonoBehaviour
{
    public PuyoState currentState;
    public bool isActive = true;

    // Update is called once per frame
    private void FixedUpdate()
    {
        if (!isActive) { return; }
        currentState?.Run();
    }

    public void SetState(PuyoState newState){
        if (!isActive) { return; }
//        Debug.Log($"NEW {newState.GetType()}\tCURRENT {currentState?.GetType()}");
        if(newState.GetType() == currentState?.GetType()) return;
//        Debug.Log($"NEW STATE: {newState.GetType()}");

        currentState?.Exit();
        currentState = newState;
        newState.Enter();
    }
}
