public interface State
{
    public void Enter();
    public void Run();
    public void Exit();
}