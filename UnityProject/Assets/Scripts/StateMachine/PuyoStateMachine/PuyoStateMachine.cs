using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PuyoStates;

public class PuyoStateMachine : StateMachine {
    public Puyo puyo;

    void Awake()
    {
        puyo ??= GetComponent<Puyo>();
    }
   
}