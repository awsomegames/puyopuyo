
using System.Collections;
using UnityEngine;
using PuyoStates;
using System.Collections.Generic;

namespace PuyoStates {
    
    public class PuyoCheckingForLinksState : PuyoState
    {
        public PuyoCheckingForLinksState(Puyo puyo) : base(puyo) { }
        float delay = 0.1f;
        public float timer = 0f;

        public override void Enter()
        {
            puyo.partner?.puyoStateMachine.SetState(new PuyoCheckingForLinksState(puyo.partner));
            puyo.partner = null;

            puyo.rbody.velocity = new Vector2(0, 0);
            puyo.currentGridCell.puyoInCell = puyo;

            puyo.SetName();
            puyo.rbody.bodyType = RigidbodyType2D.Kinematic;

            puyo.boardManager.canPlayerControlPuyoPuyo = false;

            timer = 0;
            puyo.AdujstPositionToCell();
            CheckForLinks();
        }

        public override void Run()
        {
            timer += Time.fixedDeltaTime;
            if(timer >= delay){
                if(puyo.boardManager.CanPuyoFreeFall(puyo)){
                    puyo.puyoStateMachine.SetState(new PuyoFreeFallingState(puyo));
                }
                else{
                    puyo.puyoStateMachine.SetState(new PuyoStaticOnBoardState(puyo));
                }
            }
        }

        public override void Exit()
        {
            timer = 0;
            return;
        }

        private void CheckForLinks()
        {
            List<Puyo> adyacentPuyos = new List<Puyo>();

            Puyo puyoBelow = puyo.boardManager.gridManager.GetPuyoBelow(puyo);
            if(puyoBelow?.typeID == puyo.typeID &&
                (puyoBelow?.puyoStateMachine.currentState.GetType() == typeof(PuyoStaticOnBoardState) ||
                puyoBelow?.puyoStateMachine.currentState.GetType() == typeof(PuyoCheckingForLinksState)))
            {
                adyacentPuyos.Add(puyoBelow);
                puyo.animator.Play("BottomLink.LinkBottom");
                puyoBelow.animator.Play("TopLink.LinkTop");
            }
            Puyo puyoAtLeft = puyo.boardManager.gridManager.GetPuyoAtLeft(puyo);
            if(puyoAtLeft?.typeID == puyo.typeID &&
                (puyoAtLeft?.puyoStateMachine.currentState.GetType() == typeof(PuyoStaticOnBoardState) ||
                puyoAtLeft?.puyoStateMachine.currentState.GetType() == typeof(PuyoCheckingForLinksState)))
            {
                adyacentPuyos.Add(puyoAtLeft);
                puyo.animator.Play("LeftLink.LinkLeft");
                puyoAtLeft.animator.Play("RightLink.LinkRight");
            }

            Puyo puyoAtRight = puyo.boardManager.gridManager.GetPuyoAtRight(puyo);
            if(puyoAtRight?.typeID == puyo.typeID &&
                (puyoAtRight?.puyoStateMachine.currentState.GetType() == typeof(PuyoStaticOnBoardState) ||
                puyoAtRight?.puyoStateMachine.currentState.GetType() == typeof(PuyoCheckingForLinksState)))
            {
                adyacentPuyos.Add(puyoAtRight);
                puyo.animator.Play("RightLink.LinkRight");
                puyoAtRight.animator.Play("LeftLink.LinkLeft");
            }

            Puyo puyoAbove = puyo.boardManager.gridManager.GetPuyoAbove(puyo);
            if(puyoAbove?.typeID == puyo.typeID &&
                (puyoAbove?.puyoStateMachine.currentState.GetType() == typeof(PuyoStaticOnBoardState) ||
                puyoAbove?.puyoStateMachine.currentState.GetType() == typeof(PuyoCheckingForLinksState)))
            {
                adyacentPuyos.Add(puyoAbove);
                puyo.animator.Play("TopLink.LinkTop");
                puyoAbove.animator.Play("BottomLink.LinkBottom");
            }

            AddPuyosToComboGroup(puyo, adyacentPuyos.ToArray());
        }

        private void AddPuyosToComboGroup(Puyo puyo, params Puyo[] adyacentPuyos){
            if(adyacentPuyos.Length == 0) return;
            PuyoComboGroup newComboGroup = new PuyoComboGroup(puyo.boardManager);
            newComboGroup.Add(puyo);
            for (int i = 0; i < adyacentPuyos.Length; i++)
            {
                Puyo puyoi = adyacentPuyos[i];
//                Debug.Log("Adyacent Puyo: " + puyoi.name);
                if(puyoi.puyoComboGroup != null){
//                    Debug.Log("Adyacent Puyo has combo group!: " + puyoi.name);
                    JoinComboGroups(newComboGroup, puyoi.puyoComboGroup);
                }
                else {
                    newComboGroup.Add(puyoi);
                }
            }
            puyo.boardManager.AddNewPuyoComboGroup(newComboGroup);
        }

        private void JoinComboGroups(PuyoComboGroup group1, PuyoComboGroup group2){
            foreach(Puyo p in group2.puyos){
                group1.Add(p);
            }
            puyo.boardManager.RemovePuyoComboGroup(group2);
        }

    }
}