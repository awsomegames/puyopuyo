using System.Collections.Generic;
using UnityEngine;

namespace PuyoStates{
    public class PuyoStaticOnBoardState : PuyoState
    {
        public PuyoStaticOnBoardState(Puyo puyo) : base(puyo) { }

        public override void Enter()
        {
            puyo.Unhighlight();
            if(!puyo.alreadyStatic){
                puyo.alreadyStatic = true;
            }

            puyo.AdujstPositionToCell();
            if(IsPuyoAboveBoardBoundaries(puyo)){
                puyo.boardManager.GameOver();
            }
        }

        public bool IsPuyoAboveBoardBoundaries(Puyo puyo){
            return puyo.currentGridCell.y >= puyo.boardManager.gridManager.gridSize.y - 2;
        }

        public override void Run()
        {
            CheckForFreeFall();
            return;
        }

        public override void Exit()
        {
            return;
        }

        public override void CheckForFreeFall()
        {
            if(puyo.boardManager.CanPuyoFreeFall(puyo)){
                puyo.puyoStateMachine.SetState(new PuyoFreeFallingState(puyo));
            }
        }
    }
}