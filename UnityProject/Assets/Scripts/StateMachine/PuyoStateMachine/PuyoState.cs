
using UnityEngine;

namespace PuyoStates {
    public abstract class PuyoState : State
    {
        protected Puyo puyo;

        public PuyoState(Puyo puyo)
        {
            this.puyo = puyo;
        }

        public abstract void Enter();

        public abstract void Exit();

        public abstract void Run();

        public virtual void Moved(Vector3 movement){
            return;
        }

        public virtual void CheckForFreeFall(){
            return;
        }

        public virtual void EnteredCollision2D(Collision2D collision){
            return;
        }

        public virtual void RotateToDirection(Vector3 newDir){

        }
    }
}