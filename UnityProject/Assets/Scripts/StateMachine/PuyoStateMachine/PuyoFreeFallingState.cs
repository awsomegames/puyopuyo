using System.Collections.Generic;
using UnityEngine;

namespace PuyoStates {
    public class PuyoFreeFallingState : PuyoState
    {
        public PuyoFreeFallingState(Puyo puyo) : base(puyo) { }
        private bool hasStartedFalling = false;
        public override void Enter()
        {
            puyo.rbody.bodyType = UnityEngine.RigidbodyType2D.Dynamic;
            puyo.rbody.gravityScale = 1.5f;
            puyo.UnlinkAll();
            puyo.puyoComboGroup?.Remove(puyo);
            // Cancel linked animations in puyos linked to this one
            puyo.boardManager.gridManager.GetPuyoAtLeft(puyo)?.animator.Play("RightLink.UnlinkRight");
            puyo.boardManager.gridManager.GetPuyoAtRight(puyo)?.animator.Play("LeftLink.UnlinkLeft");

        }

        public override void Run()
        {
            if(!hasStartedFalling && puyo.rbody.velocity.y > 0){
                hasStartedFalling = true;
            }
            if(hasStartedFalling && puyo.alreadyStatic && puyo.rbody.velocity.y == 0){
                puyo.Land(tolerance: false);
            }
        }

        public override void Exit()
        {
            return;
        }

        public override void EnteredCollision2D(Collision2D collision2D){
            if(collision2D.collider.CompareTag("Floor")){
                LandAllPuyosAbove();
            }
            else if(collision2D.collider.CompareTag("Puyo")){
                Puyo collidedPuyo = collision2D.collider.gameObject.GetComponent<Puyo>();
                if(puyo.boardManager.gridManager.PuyoConnectsToFloor(collidedPuyo)){
                    LandAllPuyosAbove();
                }
            }
        }

        public void LandAllPuyosAbove(){
            puyo.Land(tolerance: false);
            Puyo currentPuyo = puyo;
            int safeCounter = 0;
            while(true){
                Puyo puyoAbove = currentPuyo.boardManager.gridManager.GetPuyoAbove(currentPuyo);
                if(puyoAbove != null){
                    //Debug.Log("Puyo above! " + puyoAbove.name);
                    // Bugfix attempt on puyos falling inside puyos below when falling from high
                    // It dind't work, so i'll just leave the Debug.Log in case it happens
                    // It was fixed adjusting hitboxes. Not quite "the solution", but a
                    // functional work around.
                    foreach(Puyo p in puyo.boardManager.puyosOnBoard){
                        if(p != currentPuyo && p.currentGridCell == currentPuyo.currentGridCell){
                            Debug.Log($"WTF {p.name}");
                            /*p.RemoveGridCellCollision(p.currentGridCell);
                            p.AddGridCellCollision(puyo.boardManager.gridManager.HasGridCellAbove(currentPuyo));
                            p.currentGridCell = p.GetLowestGridCellInList();
                            p.AdujstPositionToCell();*/
                        }
                    }
                    puyoAbove.puyoStateMachine.SetState(new PuyoLandingState(puyoAbove, tolerance: false));
                    currentPuyo = puyoAbove;
                }
                else{
                    break;
                }
                safeCounter++;
                if(safeCounter > 12){
                    break;
                }
            }
        }

    }
}