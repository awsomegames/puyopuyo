
using System.Collections.Generic;
using UnityEngine;

namespace PuyoStates
{
    public class PuyoControlFallingState : PuyoState
    {
        public PuyoControlFallingState(Puyo puyo) : base(puyo) { }

        public override void Enter()
        {
            puyo.partner?.puyoStateMachine.SetState(new PuyoControlFallingState(puyo.partner));
            puyo.rbody.gravityScale = 0;
            float fallingSpeed = puyo.boardManager.currentPuyoFallingSpeed;
            puyo.rbody.velocity = new Vector2(0, -fallingSpeed);
            puyo.boardManager.canPlayerControlPuyoPuyo = true;

            puyo.animator.Play("Base Layer.Idle");
        }

        public override void Run()
        {
            // CHECK Empty?
            return;
        }

        public override void Exit()
        {
            puyo.boardManager.CancelFastFall();
            return;
        }

        public override void EnteredCollision2D(Collision2D collision2D){
            if (collision2D.collider.CompareTag("Floor"))
            {
                LandPuyoPuyo();
                return;
            }
            if(collision2D.collider.CompareTag("Puyo")){
                Puyo puyoBelow = puyo.boardManager.gridManager.GetPuyoBelow(puyo);
                Puyo collisionedPuyo = collision2D.collider.GetComponent<Puyo>();
                if (puyoBelow != null && collisionedPuyo != null &&
                    puyoBelow == collisionedPuyo && puyo.boardManager.gridManager.PuyoConnectsToFloor(puyoBelow))
                {
                    LandPuyoPuyo();
                }
            }
            return;
        }

        private void LandPuyoPuyo(){
            puyo.Land(tolerance: true);
            if( puyo.partner != null &&
                puyo.partner.transform.localPosition != puyo.transform.localPosition + puyo.partnerRelativePosition)
            {
                puyo.partner.transform.localPosition = puyo.transform.localPosition + puyo.partnerRelativePosition;
                puyo.partner.Land(tolerance: true);
            }
        }        

        public override void Moved(Vector3 movement)
        {
            puyo.transform.localPosition += movement;
            puyo.transform.localPosition = new Vector3(
                (int)(puyo.currentGridCell.transform.localPosition.x) + 0.5f,
                puyo.transform.localPosition.y,
                puyo.transform.localPosition.z
            );
        }

        public override void RotateToDirection(Vector3 newDir)
        {
            puyo.ChangePartnerRelativePosition(newDir);
        }
    }
}