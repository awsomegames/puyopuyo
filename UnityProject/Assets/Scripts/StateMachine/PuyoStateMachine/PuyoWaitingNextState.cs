using System.Collections;
using System.Collections.Generic;
using PuyoStates;
using UnityEngine;

public class PuyoWaitingNextState : PuyoState
{
    public PuyoWaitingNextState(Puyo puyo) : base(puyo) { }

    public override void Enter()
    {
        puyo.rbody.gravityScale = 0f;
        puyo.rbody.velocity = Vector2.zero;
    }

    public override void Run()
    {
        return;
    }

    public override void Exit()
    {
        return;
    }

}
