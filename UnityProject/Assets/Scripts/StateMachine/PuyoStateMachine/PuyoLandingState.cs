
using System.Collections;
using UnityEngine;
using PuyoStates;

namespace PuyoStates {
    
    public class PuyoLandingState : PuyoState
    {
        public PuyoLandingState(Puyo puyo, bool tolerance) : base(puyo) {
            this.tolerance = tolerance;
        }
        bool tolerance;
        float onTopOfPuyoDelayTolerance = 0.7f;
        float noMovementTolerance = 0.25f;
        float noMovementTimer = 0f;
        public float timer = 0f;

        public override void Enter()
        {
            puyo.partner?.puyoStateMachine.SetState(new PuyoLandingState(puyo.partner, this.tolerance));
            puyo.rbody.velocity = new Vector2(0, 0);
            timer = 0;
            noMovementTimer = 0f;
            puyo.animator.Play("Land");
            if(!tolerance){
                puyo.StopFalling();
            }
        }

        public override void Run()
        {
            timer += Time.fixedDeltaTime;
            noMovementTimer += Time.fixedDeltaTime;
            if(timer >= onTopOfPuyoDelayTolerance || noMovementTimer > noMovementTolerance){
                puyo.StopFalling();
            }
        }

        public override void Exit()
        {
            timer = 0;
            return;
        }

        public override void Moved(Vector3 movement)
        {
            puyo.transform.localPosition += movement;
            puyo.transform.localPosition = new Vector3(
                (int)(puyo.currentGridCell.transform.localPosition.x) + 0.5f,
                puyo.transform.localPosition.y,
                puyo.transform.localPosition.z
            );
            noMovementTimer = 0f;
            if(puyo.boardManager.CanPuyoControlFall(puyo)){
                puyo.puyoStateMachine.SetState(new PuyoControlFallingState(puyo));
                puyo.partner.puyoStateMachine.SetState(new PuyoControlFallingState(puyo.partner));
            }
        }
    }
}