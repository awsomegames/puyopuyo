using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using AudioInfo;
using UnityEngine.SceneManagement;

public class AudioManager : MonoBehaviour
{
    public static AudioManager Instance;
    public AudioSource audioSource;
    [SerializeField] private AudioList audios;
    private float maxVolume;
    public event System.Action<IntroLoopAudio> OnAudioFinished;

    void Awake()
    {
        Instance = this;
        audioSource ??= GetComponent<AudioSource>();
        maxVolume = audioSource.volume;
    }
    
    void Start()
    {

        switch(SceneManager.GetActiveScene().name){
            case "TitleScreen":
                PlayAudio("TitleScreen");
                break;
            case "MainPuyoGame":
                PlayAudio("Gameplay");
                break;
        }
        
    }

    public void PlayAudio(string audioName){
        PlayAudio(audios[audioName].introLoopAudio);
    }

    public void PlayAudiosInOrder(string [] audioNames){
        List<AudioData> audiosData = new List<AudioData>();
        foreach(string name in audioNames){
            audiosData.Add(audios[name]);
        }
        StopCoroutine("PlayAudiosInOrderCorutine");
        StartCoroutine(PlayAudiosInOrderCorutine(audiosData));
    }

    public IEnumerator PlayAudiosInOrderCorutine(List<AudioData> audiosData){
        foreach(AudioData ad in audiosData){
            IntroLoopAudio audio = ad.introLoopAudio;
            switch(audio.audioMode){
            case AudioMode.PlayOnlyIntro:
                audioSource.clip = audio.introClip;
                audioSource.loop = false;
                audioSource.Play();
                yield return new WaitWhile(() => audioSource.isPlaying);
                OnAudioFinished?.Invoke(audio);
                break;
            case AudioMode.PlayOnlyLoop:
                audioSource.clip = audio.loopClip;
                audioSource.loop = true;
                audioSource.Play();
                yield return null;
                break;
            case AudioMode.PlayIntroAndLoop:
                audioSource.clip = audio.introClip;
                audioSource.loop = false;
                audioSource.Play();
                yield return new WaitWhile(() => audioSource.isPlaying);
                audioSource.clip = audio.loopClip;
                audioSource.loop = true;
                audioSource.Play();
                break;
            }
        }
    }

    public void PlayAudio(IntroLoopAudio audio){
        switch(audio.audioMode){
            case AudioMode.PlayOnlyIntro:
                StartCoroutine(PlayIntroAudioCorutine(audio));
                break;
            case AudioMode.PlayOnlyLoop:
                StartCoroutine(PlayLoopAudioCorutine(audio));
                break;
            case AudioMode.PlayIntroAndLoop:
                StartCoroutine(PlayIntroLoopAudioCorutine(audio));
                break;
        }
    }

    public IEnumerator PlayIntroAudioCorutine(IntroLoopAudio audio){
        audioSource.clip = audio.introClip;
        audioSource.loop = false;
        audioSource.Play();
        yield return new WaitWhile(() => audioSource.isPlaying);
        OnAudioFinished?.Invoke(audio);
    }

    public IEnumerator PlayLoopAudioCorutine(IntroLoopAudio audio){
        audioSource.clip = audio.loopClip;
        audioSource.loop = true;
        audioSource.Play();
        yield return null;
    }

    public IEnumerator PlayIntroLoopAudioCorutine(IntroLoopAudio audio){
        audioSource.clip = audio.introClip;
        audioSource.loop = false;
        audioSource.Play();
        yield return new WaitWhile(() => audioSource.isPlaying);
        audioSource.clip = audio.loopClip;
        audioSource.loop = true;
        audioSource.Play();
    }

    public void GamePaused(){
        audioSource.volume = 0.3f * maxVolume;
    }

    public void GameUnpaused(){
        audioSource.volume = maxVolume;
    }

}
