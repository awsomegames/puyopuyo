using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class TitleScreenUI : MonoBehaviour
{
    [SerializeField] private GameObject controlsPane;
    [SerializeField] private GameObject creditsPane;
    [SerializeField] private GameObject controlsButton;
    [SerializeField] private GameObject controlsCloseButton;
    [SerializeField] private GameObject creditsButton;
    [SerializeField] private GameObject creditsCloseButton;
    [SerializeField] private GameObject startButton;

    private void Start()
    {
        controlsPane.SetActive(false);
        creditsPane.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(startButton);
    }

    public void CloseControlsPane(){
        controlsPane.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(controlsButton);
    }

    public void OpenControlsPane(){
        controlsPane.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(controlsCloseButton);
    }

    public void CloseCreditsPane(){
        creditsPane.SetActive(false);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsButton);
    }

    public void OpenCreditsPane(){
        creditsPane.SetActive(true);
        EventSystem.current.SetSelectedGameObject(null);
        EventSystem.current.SetSelectedGameObject(creditsCloseButton);
    }
}
