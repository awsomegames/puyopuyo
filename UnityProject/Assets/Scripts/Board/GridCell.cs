using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridCell : MonoBehaviour
{
    public int x;
    public int y;
    public Puyo puyoInCell = null;

    public void SetGirdCoordinates(int x, int y){
        this.x = x;
        this.y = y;
        name = $"Cell [{x}, {y}]";
    }

    public void PositionInGrid(){
        transform.localPosition = new Vector3(x, y, transform.localPosition.z);
        transform.localScale = new Vector3(1, 1, 1);
    }
}
