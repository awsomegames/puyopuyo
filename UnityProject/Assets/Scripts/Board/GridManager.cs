using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GridManager : MonoBehaviour
{
    public Vector2Int gridSize = new Vector2Int(6, 12);
    [SerializeField] GameObject gridCellPrefab = null;
    private GridCell[,] gridCellsMatrix = null;
    public float gridScale = 1;
    // Start is called before the first frame update
    void Start()
    {
        gridSize += new Vector2Int(0, 2);
        GenerateGrid();
        gridScale = transform.parent.localScale.x;
    }

    private void GenerateGrid(){
        gridCellsMatrix = new GridCell[gridSize.x, gridSize.y];
        for (int x = 0; x < gridSize.x; x++)
        {
            for (int y = 0; y < gridSize.y; y++)
            {
                GridCell gridCell = Instantiate(gridCellPrefab).GetComponent<GridCell>();
                gridCell.SetGirdCoordinates(x, y);
                gridCell.transform.parent = transform;
                gridCell.PositionInGrid();
                gridCellsMatrix[x, y] = gridCell;
            }
        }
    }

    #region Left Puyo Checking
    public Puyo GetPuyoAtLeft(Puyo puyo){
        return GetPuyoAtLeft(puyo.currentGridCell);
    }
    public Puyo GetPuyoAtLeft(GridCell gridCell){
        if(HasGridCellAtLeft(gridCell)){
            GridCell leftGridCell = gridCellsMatrix[gridCell.x - 1, gridCell.y];
            return leftGridCell.puyoInCell;
        }
        return null;
    }

    public GridCell HasGridCellAtLeft(Puyo puyo){
        return HasGridCellAtLeft(puyo.currentGridCell);
    }
    public GridCell HasGridCellAtLeft(GridCell gridCell)
    {
        if(gridCell.x - 1 < 0){
            return null;
        }
        return gridCellsMatrix[gridCell.x - 1, gridCell.y];
    }
    #endregion

    #region Right Puyo Checking
    public Puyo GetPuyoAtRight(Puyo puyo)
    {
        return GetPuyoAtRight(puyo.currentGridCell);
    }
    public Puyo GetPuyoAtRight(GridCell gridCell)
    {
        if(HasGridCellAtRight(gridCell)){
            GridCell rightGridCell = gridCellsMatrix[gridCell.x + 1, gridCell.y];
            return rightGridCell.puyoInCell;
        }
        return null;
    }

    public GridCell HasGridCellAtRight(Puyo puyo)
    {
        return HasGridCellAtRight(puyo.currentGridCell);

    }
    public GridCell HasGridCellAtRight(GridCell gridCell)
    {
        if(gridCell.x + 1 >= gridSize.x){
            return null;
        }
        return gridCellsMatrix[gridCell.x + 1, gridCell.y];;
    }
    #endregion

    #region Below Puyo Checking
    public GridCell HasGridCellBelow(Puyo puyo)
    {
        return HasGridCellBelow(puyo.currentGridCell);

    }
    public GridCell HasGridCellBelow(GridCell gridCell)
    {
        if(gridCell.y - 1 < 0){
            return null;
        }
        return gridCellsMatrix[gridCell.x, gridCell.y - 1];
    }

    public Puyo GetPuyoBelow(Puyo puyo)
    {
        return GetPuyoBelow(puyo.currentGridCell);
    }
    public Puyo GetPuyoBelow(GridCell gridCell)
    {
        if(HasGridCellBelow(gridCell)){
            GridCell belowGridCell = gridCellsMatrix[gridCell.x, gridCell.y - 1];
            return belowGridCell.puyoInCell;
        }
        return null;
    }
    #endregion

    #region Above Puyo Checking
    public GridCell HasGridCellAbove(Puyo puyo)
    {
        return HasGridCellAbove(puyo.currentGridCell);

    }
    public GridCell HasGridCellAbove(GridCell gridCell)
    {
        if(gridCell.y + 1 >= gridSize.y){
            return null;
        }
        return gridCellsMatrix[gridCell.x, gridCell.y + 1];
    }

    public Puyo GetPuyoAbove(Puyo puyo)
    {
        return GetPuyoAbove(puyo.currentGridCell);
    }
    public Puyo GetPuyoAbove(GridCell gridCell)
    {
        if(HasGridCellAbove(gridCell)){
            GridCell aboveGridCell = gridCellsMatrix[gridCell.x, gridCell.y + 1];
            return aboveGridCell.puyoInCell;
        }
        return null;
    }
    #endregion
    
    public bool PuyoConnectsToFloor(Puyo collidedPuyo)
    {
        Puyo puyo = collidedPuyo;
        while(true){
            if(!HasGridCellBelow(puyo)){
                return true;
            }
            Puyo puyoBelow = GetPuyoBelow(puyo);
            if(puyoBelow == null){
                return false;
            }
            puyo = puyoBelow;
        }
    }

    public GridCell GetCellAtDirection(GridCell cell, Vector3 dir){
        if (dir == Vector3.up){
            return HasGridCellAbove(cell);
        }
        else if (dir == Vector3.down){
            return HasGridCellBelow(cell);
        }
        else if (dir == Vector3.left){
            return HasGridCellAtLeft(cell);
        }
        else if (dir == Vector3.right){
            return HasGridCellAtRight(cell);
        }
        throw new System.Exception(
            "Not supported direction. The method only allows Vector3.up, " +
            "Vector3.left, Vector3.down and Vector3.right."
        );
    }

    public Puyo GetPuyoAtDirection(Puyo puyo, Vector3 dir){
        if (dir == Vector3.up){
            return GetPuyoAbove(puyo.currentGridCell);
        }
        else if (dir == Vector3.down){
            return GetPuyoBelow(puyo.currentGridCell);
        }
        else if (dir == Vector3.left){
            return GetPuyoAtLeft(puyo.currentGridCell);
        }
        else if (dir == Vector3.right){
            return GetPuyoAtRight(puyo.currentGridCell);
        }
        throw new System.Exception(
            "Not supported direction. The method only allows Vector3.up, " +
            "Vector3.left, Vector3.down and Vector3.right."
        );
    }
}
