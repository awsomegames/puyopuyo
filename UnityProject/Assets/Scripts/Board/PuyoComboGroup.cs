using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PuyoStates;

[System.Serializable]
public class PuyoComboGroup
{
    public List<Puyo> puyos = null;
    public BoardManager boardManager = null;

    public PuyoComboGroup(BoardManager boardManager){
        this.boardManager = boardManager;
        puyos = new List<Puyo>();
    }

    public int Count => puyos.Count;


    public void Add(Puyo puyo) {
        if(puyos.Contains(puyo)) return;
        puyos.Add(puyo);
        puyo.puyoComboGroup = this;
    }
    public void Remove(Puyo puyo){
        puyos.Remove(puyo);
        puyo.puyoComboGroup = null;
        if(puyos.Count <= 1){
            foreach(Puyo p in puyos){
                p.puyoComboGroup = null;
            }
            puyos.Clear();
            boardManager.RemovePuyoComboGroup(this);
        }
    }

    public int CheckCombo(){
        // Debug.Log("Checking combo: " + puyos.Count);
        if(puyos.Count >= 4){
            int puyosInCombo = puyos.Count;
            DestroyComboGroup();
            return puyosInCombo;
        }
        return -1;
    }

    public void DestroyComboGroup(){
        // TODO: Destroy combo and award points
        Debug.Log("Puyo combo! " + puyos.Count);
        foreach(Puyo puyo in puyos.ToArray()){
            puyo.puyoStateMachine.SetState(new PuyoComboGroupDestroyingState(puyo));
        }
        boardManager.RemovePuyoComboGroup(this);
    }
    
}