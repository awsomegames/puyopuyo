using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PuyoStates;
using Operations;

public partial class BoardManager : MonoBehaviour
{
    [Header("References")]
    [SerializeField] public GridManager gridManager = null;
    [SerializeField] private GameObject puyoPrefab = null;
    [SerializeField] private Transform parentForPuyos = null;
    [SerializeField] private GameObject gameOverObject = null;
    [SerializeField] private AudioManager audioManager = null;
    public Transform puyoSpawnPoint;
    public Transform nextPuyoSpawnPoint;
    
    [Header("Puyo Types")]
    public List<GameObject> puyoTypes = null;
    public int puyoTypesToChoose = 4;
    [Header("Board Settings")]
    [SerializeField] private float initialPuyoFallingSpeed = 1;
    public float currentPuyoFallingSpeed = 0;
    [SerializeField] private float fastFallSpeedMultiplier = 2; // CHECK GameManager?
    public bool canPlayerControlPuyoPuyo = false;
    [Header("Difficulty settings")]
    [Tooltip("Interval in seconds in which the puyo falling speed will  be increased.")]
    [SerializeField] private float increaseSpeedInterval = 10f;
    [Tooltip("The speed increase to add at each interval.")]
    [SerializeField] private float speedIncrease = 0.1f;

    public List<Puyo> nextPuyos = null;
    public Puyo currentPuyo = null;
    public Puyo partnerPuyo = null;
    public List<PuyoComboGroup> puyoComboGroups = null;
    public List<Puyo> puyosOnBoard = null;
    public enum BoardPuyoState
    {
        ControllingPuyo,
        CheckingForCombos,
        AllStatic,
        NONE
    }
    public BoardPuyoState boardPuyoState = BoardPuyoState.NONE;
    public event System.Action<int> ComboGroupDestroyed;
    public event System.Action CombosFinished;

    private void Awake()
    {
        currentPuyoFallingSpeed = initialPuyoFallingSpeed;
        puyoComboGroups = new List<PuyoComboGroup>();
        puyosOnBoard = new List<Puyo>();
        gameOverObject.SetActive(false);
        audioManager ??= FindObjectOfType<AudioManager>();
        nextPuyos = new List<Puyo>();
    }

    private void Start()
    {
        StopAllCoroutines();
        GenerateNextPuyo();
        SpawnNextPuyoOnTop();
        StartCoroutine(WaitForPuyosToBePositionedByPlayerCorutine());
    }

    #region Puyo Perform Movement
    public void MoveCurrentPuyosLeft()
    {
        MovePuyoLeft(currentPuyo);
        MovePuyoLeft(currentPuyo.partner);
    }

    private void MovePuyoLeft(Puyo puyo)
    {
        GridCell prevGridCell = puyo.currentGridCell;
        puyo.AddGridCellCollision(gridManager.HasGridCellAtLeft(puyo));
        puyo.RemoveGridCellCollision(prevGridCell);
        puyo.puyoStateMachine.currentState.Moved(Vector3.left);

    }

    public void MoveCurrentPuyosRight()
    {
        MovePuyoRight(currentPuyo);
        MovePuyoRight(currentPuyo.partner);
    }

    private void MovePuyoRight(Puyo puyo)
    {
        GridCell prevGridCell = puyo.currentGridCell;
        puyo.AddGridCellCollision(gridManager.HasGridCellAtRight(puyo));
        puyo.RemoveGridCellCollision(prevGridCell);
        puyo.puyoStateMachine.currentState.Moved(Vector3.right);
    }

    public void RotateCurrentPuyoClockwise()
    {
        Vector3 newRelativePosition = DirectionCycle.GetNextClockwiseDirection(currentPuyo.partnerRelativePosition);
        currentPuyo.puyoStateMachine.currentState.RotateToDirection(newRelativePosition);
        return;
    }

    public void RotateCurrentPuyoCounterClockwise()
    {
        Vector3 newRelativePosition = DirectionCycle.GetNextCounterClockwiseDirection(currentPuyo.partnerRelativePosition);
        currentPuyo.puyoStateMachine.currentState.RotateToDirection(newRelativePosition);
        return;
    }

    public void FastFall()
    {
        FastFallPuyo(currentPuyo);
        FastFallPuyo(currentPuyo.partner);
    }

    private void FastFallPuyo(Puyo puyo)
    {
        if (puyo.puyoStateMachine.currentState.GetType() == typeof(PuyoControlFallingState))
        {
            puyo.rbody.velocity = new Vector2(0, -currentPuyoFallingSpeed * fastFallSpeedMultiplier);
        }
    }

    public void CancelFastFall()
    {
        CancelFastFallPuyo(currentPuyo);
        CancelFastFallPuyo(currentPuyo.partner);
    }

    public void CancelFastFallPuyo(Puyo puyo)
    {
        if (puyo.puyoStateMachine.currentState.GetType() == typeof(PuyoControlFallingState))
        {
            puyo.rbody.velocity = new Vector2(0, -currentPuyoFallingSpeed);
        }
    }
    #endregion

    #region Puyo Can Perform Movement?
    public bool CanPuyosMoveLeft()
    {
        return CanPuyoMoveLeft(currentPuyo) && CanPuyoMoveLeft(currentPuyo.partner);
    }

    private bool CanPuyoMoveLeft(Puyo puyo)
    {
        if (puyo?.currentGridCell == null) return false;

        if (!gridManager.HasGridCellAtLeft(puyo))
        {
            return false;
        }
        else
        {
            Puyo puyoAtLeft = gridManager.GetPuyoAtLeft(puyo);
            return
            puyoAtLeft == null ||
            puyoAtLeft.puyoStateMachine.currentState.GetType() != typeof(PuyoStaticOnBoardState);
        }
    }

    public bool CanPuyosMoveRight()
    {
        return CanPuyoMoveRight(currentPuyo) && CanPuyoMoveRight(currentPuyo.partner);
    }

    public bool CanPuyoMoveRight(Puyo puyo)
    {
        if (puyo?.currentGridCell == null) return false;

        if (!gridManager.HasGridCellAtRight(puyo))
        {
            return false;
        }
        else
        {
            Puyo puyoAtRight = gridManager.GetPuyoAtRight(puyo);
            return
            puyoAtRight == null ||
            puyoAtRight.puyoStateMachine.currentState.GetType() != typeof(PuyoStaticOnBoardState);
        }
    }

    public bool CanPuyoControlFall(Puyo puyo)
    {
        return
        gridManager.HasGridCellBelow(puyo) &&
        (gridManager.GetPuyoBelow(puyo) == null ||
        (gridManager.GetPuyoBelow(puyo).puyoStateMachine.currentState.GetType() != typeof(PuyoStaticOnBoardState) &&
        !gridManager.PuyoConnectsToFloor(gridManager.GetPuyoBelow(puyo))));
    }

    public bool CanPuyoFreeFall(Puyo puyo)
    {
        if (gridManager.HasGridCellBelow(puyo) == null)
        {
            return false;
        }
        Puyo puyoBelow = gridManager.GetPuyoBelow(puyo);
        if (puyoBelow == null)
        {
            return true;
        }
        if (puyoBelow.puyoStateMachine.currentState.GetType() == typeof(PuyoFreeFallingState))
        {
            return true;
        }
        return false;
    }

    public bool CanPuyoRotateClockwise()
    {
        Vector3 nextDir = DirectionCycle.GetNextClockwiseDirection(currentPuyo.partnerRelativePosition);
        return CanRotateToDirection(nextDir);
    }

    public bool CanPuyoRotateCounterClockwise()
    {
        Vector3 nextDir = DirectionCycle.GetNextCounterClockwiseDirection(currentPuyo.partnerRelativePosition);
        return CanRotateToDirection(nextDir);
    }

    public bool CanRotateToDirection(Vector3 nextDir){
        Vector3 currentDir = currentPuyo.partnerRelativePosition;

        GridCell cell1 = gridManager.GetCellAtDirection(currentPuyo.currentGridCell, nextDir);
        if (cell1 == null) { return false; }
        GridCell cell2 = gridManager.GetCellAtDirection(cell1, currentDir);
        if (cell2 == null) { return false; }

        Puyo p1 = gridManager.GetPuyoAtDirection(currentPuyo, nextDir);
        if (p1 != null) { return false; }
        Puyo p2 = gridManager.GetPuyoAtDirection(currentPuyo.partner, nextDir);
        if (p2 != null) { return false; }

        return true;
    }
    #endregion

    #region Spawn Next Puyo Operations

    public bool HavePuyosBeenPositionedByPlayer() => currentPuyo.alreadyStatic && partnerPuyo.alreadyStatic;
    public bool CanSpawnNewPuyoPuyo() => (boardPuyoState == BoardPuyoState.AllStatic);

    public IEnumerator WaitForPuyosToBePositionedByPlayerCorutine(){
        yield return new WaitUntil(HavePuyosBeenPositionedByPlayer);
        PuyoHasBeenPositionedByPlayer();
        yield return null;
    }
    public void PuyoHasBeenPositionedByPlayer()
    {
        boardPuyoState = BoardPuyoState.CheckingForCombos;
        StartCoroutine(CheckForCombosAndActionCorutine());
        StartCoroutine(SpawnNextPuyoCorutine());
    }

    private IEnumerator SpawnNextPuyoCorutine()
    {
        yield return new WaitUntil(CanSpawnNewPuyoPuyo);
        SpawnNextPuyoOnTop();
        StartCoroutine(WaitForPuyosToBePositionedByPlayerCorutine());
        yield return null;
    }

    public void SpawnNextPuyoOnTop()
    {
        Puyo nextPuyo = nextPuyos[0];
        boardPuyoState = BoardPuyoState.ControllingPuyo;

        nextPuyo.SetPuyoPosition(puyoSpawnPoint.position);
        nextPuyo.partner.SetPuyoPosition(puyoSpawnPoint.position + Vector3.up * (transform.localScale.x));

        nextPuyo.puyoStateMachine.SetState(new PuyoControlFallingState(nextPuyo));
        nextPuyo.partner.puyoStateMachine.SetState(new PuyoControlFallingState(nextPuyo.partner));

        currentPuyo = nextPuyo;
        partnerPuyo = nextPuyo.partner;

        puyosOnBoard.Add(nextPuyo);
        puyosOnBoard.Add(nextPuyo.partner);
        nextPuyo.Highlight();

        nextPuyos.Remove(nextPuyo);
        GenerateNextPuyo();
    }

    public void GenerateNextPuyo(){
        Puyo mainPuyo = CreatePuyo();
        mainPuyo.SetPuyoPosition(nextPuyoSpawnPoint.position);
        Puyo secondPuyo = CreatePuyo();
        secondPuyo.SetPuyoPosition(nextPuyoSpawnPoint.position + Vector3.up * (transform.localScale.x));
        mainPuyo.partner = secondPuyo;
        secondPuyo.partner = mainPuyo;

        mainPuyo.StartUpOnWait(Vector3.up);
        secondPuyo.StartUpOnWait(Vector3.down);

        nextPuyos.Add(mainPuyo);
    }

    private Puyo CreatePuyo()
    {
        (GameObject obj, int typeID) randomPuyoType = GetRandomPuyoType();
        GameObject randomPuyoObject = randomPuyoType.obj;
        GameObject obj = Instantiate(randomPuyoObject);
        Puyo puyo = obj.GetComponent<Puyo>();
        puyo.typeID = randomPuyoType.typeID;
        puyo.transform.parent = parentForPuyos;
        puyo.name = $"{puyo.puyoName}";
        puyo.boardManager = this;
        puyo.transform.localScale = Vector3.one;
        return puyo;
    }

    private (GameObject obj, int typeID) GetRandomPuyoType()
    {
        int r = Random.Range(0, puyoTypesToChoose);
        GameObject randomPuyo = puyoTypes[r];
        return (obj: randomPuyo, typeID: r);
    }
    #endregion

    #region Combo Group Operations
    public void AddNewPuyoComboGroup(Puyo puyo, params Puyo[] puyos)
    {
        PuyoComboGroup newComboGroup = new PuyoComboGroup(this);
        foreach (Puyo p in puyos)
        {
            newComboGroup.Add(p);
        }
        newComboGroup.Add(puyo);
        puyoComboGroups.Add(newComboGroup);
    }

    public void AddNewPuyoComboGroup(PuyoComboGroup newComboGroup)
    {
        puyoComboGroups.Add(newComboGroup);
    }

    public void RemovePuyoComboGroup(PuyoComboGroup comboGroup)
    {
        puyoComboGroups.Remove(comboGroup);
    }
    #endregion

    #region Status Checking
    public IEnumerator CheckForCombosAndActionCorutine()
    {
        yield return null;
        while (true)
        {
            if (AreAllPuyosStaticOnBoard())
            {
                //                Debug.Log("All puyos static! Checking for combos...");
                yield return null;
                if (!CheckAllComboGroups())
                {
                    break;
                }
                yield return null;
            }
            else
            {
                yield return null;
            }
        }
        //        Debug.Log("All puyos static! No more combos!");
        boardPuyoState = BoardPuyoState.AllStatic;
        CombosFinished?.Invoke();
        yield return null;
    }

    private bool AreAllPuyosStaticOnBoard()
    {
        CheckAllFreeFalls();
        foreach (Puyo puyo in puyosOnBoard)
        {
            //            Debug.Log(puyo.puyoStateMachine.currentState.GetType());
            if (puyo.puyoStateMachine.currentState.GetType() != typeof(PuyoStaticOnBoardState))
            {
                //Debug.Log($"Non static puyo! {puyo.name}");
                return false;
            }
        }
        return true;
    }

    private bool CheckAllComboGroups()
    {
        bool atLeastAComoboGroupWasDestroyed = false;
        foreach (PuyoComboGroup comboGroup in puyoComboGroups.ToArray())
        {
            int puyosInCombo = comboGroup.CheckCombo();
            if (puyosInCombo > 0)
            {
                atLeastAComoboGroupWasDestroyed = true;
                ComboGroupDestroyed?.Invoke(puyosInCombo);
            }
        }
        return atLeastAComoboGroupWasDestroyed;
    }

    public void CheckAllFreeFalls()
    {
        foreach (Puyo puyo in puyosOnBoard)
        {
            puyo?.puyoStateMachine?.currentState?.CheckForFreeFall();
        }
    }
    #endregion

    #region Game Management
    private void Update()
    {
        UpdateFallingSpeed();
    }
    public void UpdateFallingSpeed(){
        currentPuyoFallingSpeed =
            initialPuyoFallingSpeed +
            (int)(Time.timeSinceLevelLoad / increaseSpeedInterval) * speedIncrease;
    }

    public void GameOver()
    {
        Debug.Log("Game Over");
        StopAllCoroutines();
        audioManager.PlayAudiosInOrder(new string[] {"GameOver", "Gameplay"});
        StartCoroutine(ShowGameOverCorutine());
        // Disable collisions
        foreach(Puyo puyo in puyosOnBoard){
            puyo.GetComponent<BoxCollider2D>().isTrigger = true;
            puyo.puyoStateMachine.isActive = false;
            puyo.UnlinkAll();

            puyo.rbody.bodyType = RigidbodyType2D.Dynamic;
            puyo.rbody.gravityScale = 1;
            Vector2 randomDir = new Vector2(Random.Range(-1f, 1f), Random.Range(0, 5));
            puyo.rbody.AddForce(randomDir, ForceMode2D.Impulse);
        }
    }

    private IEnumerator ShowGameOverCorutine(){
        yield return new WaitForSeconds(4f);
        gameOverObject.SetActive(true);
    }

    #endregion
}