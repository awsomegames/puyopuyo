
namespace PuyoStates{
    public class PuyoComboGroupDestroyingState : PuyoState
    {
        public PuyoComboGroupDestroyingState(Puyo puyo) : base(puyo) { }
        float destroyDelay = 0.8f;

        public override void Enter()
        {
            puyo.animator.Play("Destroying");
            puyo.DestroyPuyoDelay(destroyDelay);
        }

        public override void Run()
        {
            return;
        }

        public override void Exit()
        {
            return;
        }
    }
}