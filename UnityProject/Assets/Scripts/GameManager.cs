using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance = null;
    public bool isGamePaused = false;
    private bool canPauseGame = true;
    [SerializeField] private GameObject pauseMenuCanvas = null;
    public event System.Action OnGamePaused;
    public event System.Action OnGameUnpaused;

    private void Awake()
    {
        if (Instance != null)
        {
            Destroy(gameObject);
            DontDestroyOnLoad(this);
        }
        Instance ??= this;
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void LoadNextScene(string sceneName)
    {
        Debug.Log($"Loading Scene {sceneName}...");
        SceneManager.LoadScene(sceneName);
        PauseMenu.Instance = null;
    }

    public void OnSceneLoaded(Scene scene, LoadSceneMode mode){
        pauseMenuCanvas ??= GameObject.Find("PauseMenuCanvas");
        isGamePaused = false;
        UnpauseGame();
    }

    public void PauseUnpauseGame()
    {
        if (!canPauseGame) { return; }
        if (isGamePaused)
        {
            UnpauseGame();
        }
        else
        {
            PauseGame();
        }
    }

    public void PauseGame()
    {
        // TODO: Pause stuff
        Time.timeScale = 0;
        isGamePaused = true;
        PauseMenu.Instance?.OnPause();
        AudioManager.Instance?.GamePaused();
        //OnGamePaused?.Invoke();
    }

    public void UnpauseGame()
    {
        // TODO: Unpause stuff
        Time.timeScale = 1f;
        GameManager.Instance.isGamePaused = false;
        PauseMenu.Instance?.OnUnpause();
        AudioManager.Instance?.GameUnpaused();
        //OnGameUnpaused?.Invoke();
    }

}