using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Operations {
    public class DirectionCycle
    {
        private static List<Vector3> directionList = new List<Vector3>{
            Vector3.up,
            Vector3.left,
            Vector3.down,
            Vector3.right
        };

        public static Vector3 GetNextCounterClockwiseDirection(Vector3 dir){
            int index = directionList.IndexOf(dir);
            if(index == -1){
                throw new System.Exception(
                    "Not supported direction. The method only allows Vector3.up, " +
                    "Vector3.left, Vector3.down and Vector3.right."
                );
            }
            return directionList[(index + 1) % directionList.Count];
        }

        public static Vector3 GetNextClockwiseDirection(Vector3 dir){
            int index = directionList.IndexOf(dir);
            if(index == -1){
                throw new System.Exception(
                    "Not supported direction. The method only allows Vector3.up, " +
                    "Vector3.left, Vector3.down and Vector3.right."
                );
            }
            return directionList[(directionList.Count + index - 1) % directionList.Count];
        }
    }
}


