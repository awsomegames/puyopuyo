using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PuyoStates;
using System;

public class Puyo : MonoBehaviour
{
    public Puyo partner;
    public Vector3 partnerRelativePosition = Vector3.up;
    public PuyoStateMachine puyoStateMachine = null;
    public Rigidbody2D rbody;
    public BoardManager boardManager = null;
    public GridCell currentGridCell = null;
    public bool alreadyStatic = false;
    public List<GridCell> gridCellCollisionList = null;
    public Animator animator;
    public string puyoName;
    public int typeID = -1;
    [HideInInspector] public PuyoComboGroup puyoComboGroup = null;

    internal void AdujstPositionToCell()
    {
        transform.localPosition = currentGridCell.transform.localPosition;
        transform.localPosition += new Vector3(1, 1, 0) / 2;
    }

    /*public void StartUpOnBoard(Vector3 partnerRelativePosition)
    {
        rbody ??= GetComponent<Rigidbody2D>();
        puyoStateMachine ??= GetComponent<PuyoStateMachine>();
        alreadyStatic = false;
        gridCellCollisionList = new List<GridCell>();
        animator = GetComponent<Animator>();
        puyoComboGroup = null;

        this.partnerRelativePosition = partnerRelativePosition;

        puyoStateMachine.SetState(new PuyoControlFallingState(this));
    }*/

    public void StartUpOnWait(Vector3 partnerRelativePosition){
        rbody ??= GetComponent<Rigidbody2D>();
        puyoStateMachine ??= GetComponent<PuyoStateMachine>();
        alreadyStatic = false;
        gridCellCollisionList = new List<GridCell>();
        animator = GetComponent<Animator>();
        puyoComboGroup = null;

        this.partnerRelativePosition = partnerRelativePosition;

        puyoStateMachine.SetState(new PuyoWaitingNextState(this));
    }

    public void SetPuyoPosition(Vector3 pos){
        transform.position = pos;
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        switch (collider.tag)
        {
            case "GridCell":
                GridCell collidedGridCell = collider.GetComponentInParent<GridCell>();
                AddGridCellCollision(collidedGridCell);
                break;
        }
    }
    private void OnTriggerExit2D(Collider2D other)
    {
        switch (other.tag)
        {
            case "GridCell":
                GridCell uncollidedGridCell = other.GetComponentInParent<GridCell>();
                RemoveGridCellCollision(uncollidedGridCell);
                break;
        }
    }

    public void AddGridCellCollision(GridCell gridCell){
        if(!gridCellCollisionList.Contains(gridCell)){
            gridCellCollisionList.Add(gridCell);
        }
        foreach(GridCell cell in gridCellCollisionList){
            if(cell.puyoInCell == this){
                cell.puyoInCell = null;
            }
        }
        currentGridCell = GetLowestGridCellInList();
        currentGridCell.puyoInCell = this;
        SetName();
    }

    public void RemoveGridCellCollision(GridCell gridCell){
        gridCellCollisionList.Remove(gridCell);
        if(gridCell.puyoInCell == this){
            gridCell.puyoInCell = null;
        }
        currentGridCell = GetLowestGridCellInList();
    }

    public void ChangePartnerRelativePosition(Vector3 newRelativePosition)
    {
        partnerRelativePosition = newRelativePosition;
        partner.transform.localPosition = transform.localPosition + partnerRelativePosition;

        partner.partnerRelativePosition = -newRelativePosition;
    }

    public GridCell GetLowestGridCellInList(){
        if(gridCellCollisionList.Count == 0) return null;

        GridCell lowestGridCell = gridCellCollisionList[gridCellCollisionList.Count - 1];
        foreach(GridCell gridCell in gridCellCollisionList){
            if(gridCell.y < lowestGridCell.y){
                lowestGridCell = gridCell;
            }
        }
        return lowestGridCell;
    }

    private void OnCollisionEnter2D(Collision2D collision2D)
    {
        puyoStateMachine.currentState.EnteredCollision2D(collision2D);
    }

    public void Land(bool tolerance){
        puyoStateMachine.SetState(new PuyoLandingState(this, tolerance));
    }

    public void StopFalling(){
        if(boardManager.CanPuyoFreeFall(this)){
            puyoStateMachine.SetState(new PuyoFreeFallingState(this));
        }
        else {
            puyoStateMachine.SetState(new PuyoCheckingForLinksState(this));
        }
    }

    public void SetName(){
        int x = currentGridCell.x;
        int y = currentGridCell.y;
        name = $"{puyoName} [{x}, {y}]";
    }

    public void DestroyPuyoDelay(float delayInSeconds){
        StartCoroutine(DestroyPuyoDelayCorutine(delayInSeconds));
    }

    public IEnumerator DestroyPuyoDelayCorutine(float delayInSeconds){
        yield return new WaitForSeconds(delayInSeconds);
        Destroy(gameObject);
        foreach(GridCell gridCell in gridCellCollisionList){
            gridCell.puyoInCell = null;
        }
        boardManager.puyosOnBoard.Remove(this);
    }

    public void Highlight(){
        animator.Play("Highlight.Highlight");
    }

    public void Unhighlight(){
        animator.Play("Highlight.Unchanged");
    }

    public void UnlinkAll(){
        animator.Play("TopLink.UnlinkTop");
        animator.Play("BottomLink.UnlinkBottom");
        animator.Play("LeftLink.UnlinkLeft");
        animator.Play("RightLink.UnlinkRight");
    }

}
