using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPuyoController : MonoBehaviour
{
    public BoardManager boardManager = null;
    public GridManager gridManager = null;

    void Awake()
    {
        // CHECK Needed?
        boardManager ??= transform.parent.GetComponent<BoardManager>();
    }

    public void TryMovePuyoLeft()
    {
        if(!boardManager.canPlayerControlPuyoPuyo) return;

        if(boardManager.CanPuyosMoveLeft()){
            boardManager.MoveCurrentPuyosLeft();
        }
        else{
            // TODO: Bump Puyo to the left
            Debug.Log("Puyo can't move left!");
        }
    }

    public void TryMovePuyoRight()
    {
        if(!boardManager.canPlayerControlPuyoPuyo) return;

        if(boardManager.CanPuyosMoveRight()){
            boardManager.MoveCurrentPuyosRight();
        }
        else{
            // TODO: Bump Puyo to the Right
            Debug.Log("Puyo can't move Right!");
        }
    }

    public void TryFastFall(){
        if(!boardManager.canPlayerControlPuyoPuyo) return;

        boardManager.FastFall();
    }

    public void TryCancelFastFall(){
        if(!boardManager.canPlayerControlPuyoPuyo) return;

        boardManager.CancelFastFall();
    }

    public void TryRotateClockwise()
    {
        if(!boardManager.canPlayerControlPuyoPuyo) return;

        if(boardManager.CanPuyoRotateClockwise()){
            boardManager.RotateCurrentPuyoClockwise();
        }
        else{
            // TODO: Bump Puyo to the Right
            Debug.Log("Puyo can't Rotate Clockwise!");
        }
    }

    public void TryRotateCounterClockwise()
    {
        if(!boardManager.canPlayerControlPuyoPuyo) return;

        if(boardManager.CanPuyoRotateCounterClockwise()){
            boardManager.RotateCurrentPuyoCounterClockwise();
        }
        else{
            // TODO: Bump Puyo to the Right
            Debug.Log("Puyo can't Rotate Counter Clockwise!");
        }
    }

}
