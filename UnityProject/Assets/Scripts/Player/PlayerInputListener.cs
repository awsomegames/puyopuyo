using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.UI;

public class PlayerInputListener : MonoBehaviour
{
    PlayerPuyoController playerPuyoController = null;
    [SerializeField] private PlayerInput playerInput = null;

    private void Awake()
    {
        playerPuyoController ??= GetComponent<PlayerPuyoController>();
        playerInput ??= GetComponent<PlayerInput>();
        /*GameObject eventSystemObj = GameObject.Find("EventSystem");
        InputSystemUIInputModule UIInputModule = eventSystemObj.GetComponent<InputSystemUIInputModule>();
        playerInput.uiInputModule = UIInputModule;*/
    }
    private void OnMoveLeft(InputValue input){
        if (GameManager.Instance.isGamePaused) { return; }
        playerPuyoController.TryMovePuyoLeft();
    }

    private void OnMoveRight(InputValue input){
        if (GameManager.Instance.isGamePaused) { return; }
        playerPuyoController.TryMovePuyoRight();
    }

    private void OnRotateCounterClockwise(InputValue input){
        if (GameManager.Instance.isGamePaused) { return; }
        playerPuyoController.TryRotateCounterClockwise();
    }

    private void OnRotateClockwise(InputValue input){
        if (GameManager.Instance.isGamePaused) { return; }
        playerPuyoController.TryRotateClockwise();
    }

    private void OnFastFall(InputValue input){
        if (GameManager.Instance.isGamePaused) { return; }
        float value = input.Get<float>();
        if(value == 1){
            playerPuyoController.TryFastFall();
        }
        else{
            playerPuyoController.TryCancelFastFall();
        }
    }

    private void OnPause(){
        GameManager.Instance.PauseUnpauseGame();
    }

}
