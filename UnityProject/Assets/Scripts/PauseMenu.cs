using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{

    public static PauseMenu Instance = null;
    public GameObject pauseMenu = null;
    private void Awake()
    {
        Instance = this;
    }
    private void Start()
    {
        pauseMenu.SetActive(false);
    }

    public void OnPause(){
        pauseMenu.SetActive(true);
    }

    public void OnUnpause(){
        pauseMenu.SetActive(false);
    }
}
