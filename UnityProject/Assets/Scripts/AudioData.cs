using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AudioInfo {
    [System.Serializable]
    public struct IntroLoopAudio {
        public AudioClip introClip;
        public AudioClip loopClip;
        public AudioMode audioMode;
    }

    public enum AudioMode {
        PlayOnlyIntro,
        PlayOnlyLoop,
        PlayIntroAndLoop
    }

    [System.Serializable]
    public class AudioData {
        public IntroLoopAudio introLoopAudio;
        public string audioName;
    }

    [System.Serializable]
    public class AudioList {
        public List<AudioData> audios;
        public AudioData this[string name]{
            get => GetAudioByName(name);
            set {
                AudioData audio = GetAudioByName(name);
                audio = value;
            }
        }
        private AudioData GetAudioByName(string nameToFind){
            foreach(AudioData ad in audios){
                if(ad.audioName == nameToFind){
                    return ad;
                }
            }
            return null;
        }
    }
}