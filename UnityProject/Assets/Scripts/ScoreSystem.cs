using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class ScoreSystem : MonoBehaviour
{
    [SerializeField] private BoardManager boardManager;
    [SerializeField] private AudioSource audioSource;
    [SerializeField] private TextMeshProUGUI totalScoreText;
    [SerializeField] private TextMeshProUGUI scoreText;
    [SerializeField] private TextMeshProUGUI comboCounterText;
    [SerializeField] private int scoreToSpawn5thPuyo = 5000;

    [SerializeField] private int baseScore = 100;
    private int currentScore = 0;
    private int comboCounter = 0;
    void Start()
    {
        boardManager.ComboGroupDestroyed += AddPointsByPuyosInCombo;
        boardManager.CombosFinished += FinishCombo;
        UpdateScore(0);
        scoreText.text = "";
        comboCounterText.text = "";
    }

    private void UpdateScore(int newScore)
    {
        currentScore = newScore;
        totalScoreText.text = currentScore.ToString();
        
        if(currentScore >= scoreToSpawn5thPuyo){
            boardManager.puyoTypesToChoose = 5;
        }
    }

    private void PlaySoundEffect(){
        audioSource.pitch = Mathf.Clamp(0.8f + (comboCounter - 1) * 0.15f, 0.8f, 1.4f);
        audioSource.Play();
    }

    private void AddPointsByPuyosInCombo(int puyosInCombo)
    {
        comboCounter++;
        int puyosScore = puyosInCombo * baseScore;
        int comboScore = puyosScore * comboCounter;
        scoreText.text = "" + puyosScore;
        comboCounterText.text = "x" + comboCounter;
        UpdateScore(currentScore + comboScore);
        PlaySoundEffect();
    }
    private void FinishCombo()
    {
        comboCounter = 0;
        StopCoroutine(HideComboCounterCorutine());
        StartCoroutine(HideComboCounterCorutine());
    }

    private IEnumerator HideComboCounterCorutine(){
        yield return 0.5f;
        scoreText.text = "";
        comboCounterText.text = "";
    }

}
